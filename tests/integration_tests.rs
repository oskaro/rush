use itertools::Itertools;
use rush::core::{Interpreter, Object, RushError};
use std::io::Cursor;
use test_log::test;

fn evaluate_string(program: &str) -> Result<Vec<Object>, RushError> {
    let mut reader = Cursor::new(program);
    let int = Interpreter::from_reader(&mut reader)?;
    int.evaluate()
        .into_iter()
        .filter_map_ok(|o| o)
        .collect::<Result<Vec<Object>, _>>()
}

#[test]
fn evaluate_a_simple_program() {
    let res = evaluate_string("1;").expect("evaluation should work");
    assert_eq!(res, vec![Object::from(1.0)]);
}

#[test]
fn evaluate_simple_addition() {
    let res = evaluate_string("1 + 2;").expect("evaluation should work");
    assert_eq!(res, vec![Object::from(3.0)]);
}

#[test]
fn evaluate_complex_math() {
    let res = evaluate_string("1 + 2 * 4 / 12;").expect("evaluation should work");
    assert_eq!(res, vec![Object::from(1.6666666666666665)]);
}

#[test]
fn math_with_parentheses() {
    let res = evaluate_string("(1 + 2 * 4) / 12;").expect("evaluation should work");
    assert_eq!(res, vec![Object::from(0.75)]);
}

#[test]
fn multiple_math_expressions() {
    let res = evaluate_string("(1 + 2 * 4) / 12;10/5;100 - 21;").expect("evaluation should work");
    assert_eq!(
        res,
        vec![
            Object::from(0.75),
            Object::from(2.0),
            Object::from(100.0 - 21.0)
        ]
    );
}

#[test]
fn string_concatination() {
    let res = evaluate_string(r#" "some"+ "string";"#).expect("evaluation should work");
    assert_eq!(res, vec![Object::from("somestring".to_string())]);
}

#[test]
fn no_string_to_int_concat() {
    evaluate_string(r#" "some"+ 3;"#).expect_err("string and int should not be plussed together");
}

#[test]
fn no_string_subtraction() {
    evaluate_string(r#" "some" - "some other";"#).expect_err("string should not be subtracted");
}

#[test]
fn boolean_literals_and_comparisons() {
    let res = evaluate_string(
        "true;false;true != false;false == true;true==true;10 < 100;\"foo\" != \"bar\"; \"foo\"==\"foo\";100 == 100; 100 <=100; 100<100;99<=100;100<=99;",
    )
    .expect("parsing boolean expressions");
    assert_eq!(
        res,
        vec![
            Object::from(true),
            Object::from(false),
            Object::from(true),
            Object::from(false),
            Object::from(true),
            Object::from(true),
            Object::from(true),
            Object::from(true),
            Object::from(true),
            Object::from(true),
            Object::from(false),
            Object::from(true),
            Object::from(false),
        ]
    )
}

#[test]
fn variable_assignment() {
    let res = evaluate_string("let x = 2;").expect("evaluation should work");
    assert_eq!(res, vec![]);
}

#[test]
fn variable_assignment_and_evaluation() {
    let res = evaluate_string("let x = 2;$x;").expect("evaluation should work");
    assert_eq!(res, vec![Object::from(2.0)]);
}

#[test]
fn variable_assignment_of_complex_expression() {
    let res =
        evaluate_string("let x = (2 + 10 * (100 / 5));$x;let y = \"Some\" + \" \"+ \"string\";$y;")
            .expect("evaluation should work");
    assert_eq!(
        res,
        vec![Object::from(202.0), Object::from("Some string".to_string())]
    );
}

#[test]
fn variable_reassignment() {
    let res = evaluate_string("let x = (2 + 10 * (100 / 5));$x;let y = $x;$y;")
        .expect("evaluation should work");
    assert_eq!(res, vec![Object::from(202.0), Object::from(202.0)]);
}

#[test]
fn variable_reassignment_and_expression() {
    let res = evaluate_string("let x = (2 + 10 * (100 / 5));$x;let y = $x + 100;$y;")
        .expect("evaluation should work");
    assert_eq!(res, vec![Object::from(202.0), Object::from(302.0)]);
}

#[test]
fn variable_declaration_without_initializer() {
    let res = evaluate_string("let x;").expect("evaluation should work");
    assert_eq!(res, vec![]);
}

#[test]
fn variable_declaration_without_initializer_evaluation_to_nil() {
    let res = evaluate_string("let x;$x;").expect("evaluation should work");
    assert_eq!(res, vec![Object::new_nil()]);
}

#[test]
fn variable_declaration_and_reassignment() {
    let res = evaluate_string("let x;$x;x=2;$x;").expect("evaluation should work");
    assert_eq!(
        res,
        vec![
            Object::new_nil(),
            Object::new_num(2.0),
            Object::new_num(2.0)
        ]
    );
}

#[test]
fn blocks_should_work() {
    let res = evaluate_string(
        r#"
        let x;
        $x;
        x = 2;
        $x;
        {
            let y = 10;
            $x;
            $y;
        }
    "#,
    )
    .expect("evaluation should work");
    assert_eq!(
        res,
        vec![
            Object::new_nil(),
            Object::new_num(2.0),
            Object::new_num(2.0),
            Object::new_num(10.0),
        ]
    );
}

#[test]
fn evaluation_of_commands_should_work() {
    let res = evaluate_string(
        r#"
    echo "some value";
    "#,
    )
    .expect("This should evaluate fine");
    assert_eq!(res.len(), 1);
    assert_eq!(res[0], Object::new_num(0.0));
}

#[test]
fn scopes_should_work() {
    let res = evaluate_string(
        r#"
let a = "global a";
let a = "global a";
let b = "global b";
let c = "global c";
{
  let a = "outer a";
  let b = "outer b";
  {
    let a = "inner a";
    echo a;
    echo b;
    echo c;
  }
  echo a;
  echo b;
  echo c;
}
echo a;
echo b;
echo c;
    "#,
    )
    .expect("evaluation should work");
    assert_eq!(res, vec![Object::new_str("inner_a".into()),]);
}
/*
 * #TODO fix this in future
#[test]
fn assign_to_value_of_block_should_work() {
    let res = evaluate_string(
        r#"
        let x = {
            let y = 10;
            y + 100;
        };
        x;
    "#,
    )
    .expect("evaluation should work");
    assert_eq!(res, vec![Object::new_num(110.0),]);
}
*/

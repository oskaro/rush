use std::ops::Deref;

pub type Position = (usize, usize);

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct PositionAnnotation<T> {
    inner: T,
    position: Position,
}

impl<T> PositionAnnotation<T> {
    pub fn new(inner: T, line_position: Position) -> Self {
        Self {
            inner,
            position: line_position,
        }
    }

    pub fn position(&self) -> Position {
        self.position
    }
}

impl<T> AsRef<T> for PositionAnnotation<T> {
    fn as_ref(&self) -> &T {
        &self.inner
    }
}

impl<T> Deref for PositionAnnotation<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

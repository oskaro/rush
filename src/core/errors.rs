use crate::core::interpreter::Error as RuntimeError;
use crate::core::parser::SyntaxError;
use std::fmt::Display;

#[derive(Debug, Clone)]
pub struct RushError {
    kind: RushErrorKind,
}

impl std::fmt::Display for RushError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.kind {
            RushErrorKind::IOError => write!(f, "IO error"),
            RushErrorKind::SessionExited => write!(f, "Session exited"),
            RushErrorKind::InvalidCommand => write!(f, "Invalid command"),
            RushErrorKind::ScriptNotFound => write!(f, "Script not found"),
            RushErrorKind::EncodingError => write!(f, "Encoding error"),
            RushErrorKind::LexError(e) => e.fmt(f),
            RushErrorKind::ParseError(e) => e.fmt(f),
            RushErrorKind::RuntimeError(e) => e.fmt(f),
        }
    }
}

impl RushError {
    pub fn new(kind: RushErrorKind) -> Self {
        Self { kind }
    }
}

impl Into<RushError> for LexError {
    fn into(self) -> RushError {
        RushError {
            kind: RushErrorKind::LexError(self),
        }
    }
}

impl Into<RushError> for SyntaxError {
    fn into(self) -> RushError {
        RushError {
            kind: RushErrorKind::ParseError(self),
        }
    }
}

impl Into<RushError> for RuntimeError {
    fn into(self) -> RushError {
        RushError {
            kind: RushErrorKind::RuntimeError(self),
        }
    }
}

impl Into<RushError> for std::io::Error {
    fn into(self) -> RushError {
        // TODO handle the cases here
        RushError {
            kind: RushErrorKind::IOError,
        }
    }
}

#[derive(Debug, Clone)]
pub enum RushErrorKind {
    SessionExited,
    IOError,
    InvalidCommand,
    ScriptNotFound,
    EncodingError,
    LexError(LexError),
    ParseError(SyntaxError),
    RuntimeError(RuntimeError),
}

#[derive(Debug, Clone, Copy)]
pub enum LexErrorKind {
    UnexpectedCharacter(char),
    UnterminatedString,
    EncodingError,
}

impl Display for LexErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LexErrorKind::UnexpectedCharacter(character) => {
                write!(f, "Unexpected character: {}", character)
            }
            LexErrorKind::UnterminatedString => {
                write!(f, "Unterminated string.")
            }
            LexErrorKind::EncodingError => {
                write!(f, "Encoding error.")
            }
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct LexError {
    error_type: LexErrorKind,
    pos: (usize, usize),
}

impl Display for LexError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "[{},{}] Error: {}",
            self.pos.0, self.pos.1, self.error_type
        )
    }
}

impl LexError {
    pub fn new(error_type: LexErrorKind, pos: (usize, usize)) -> Self {
        Self { error_type, pos }
    }
}

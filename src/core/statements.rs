use crate::core::expressions::Expr;

#[derive(Debug, Clone, PartialEq)]
pub enum Stmt {
    Expr(Expr),
    Decl {
        ident: String,
        initializer: Option<Expr>,
    },
    Block(Vec<Box<Stmt>>),
}

impl std::fmt::Display for Stmt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Expr(e) => write!(f, "Expr({})", e),
            Self::Decl {
                ident,
                initializer: Some(initializer),
            } => {
                write!(f, "Decl({} = {})", ident, initializer)
            }
            Self::Decl {
                ident,
                initializer: None,
            } => {
                write!(f, "Decl({})", ident)
            }
            Self::Block(stmts) => {
                writeln!(f, "Block{{[")?;
                for s in stmts {
                    writeln!(f, "    {},", s)?;
                }
                write!(f, "  ]}}")
            }
        }
    }
}

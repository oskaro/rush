use crate::core::{Command, Result, RushError, RushErrorKind};
use std::{
    io::{self, BufRead, BufReader, Write},
    path::Path,
};

pub struct Session {
    debug: bool,
    ps1: String,
}

impl Session {
    pub fn new(debug: bool) -> Self {
        Self {
            debug,
            ps1: String::from("> "),
        }
    }

    pub fn start(self) -> Result<i32> {
        loop {
            self.write_prompt()?;

            let line = self.read_line()?;

            self.handle_command(line)?;
        }
    }

    pub fn run_script<R: BufRead>(&self, buf: &mut R) -> Result<i32> {
        buf.lines()
            .map(|res| {
                let line = res.map_err(|e| {
                    eprintln!("source: {}", e.to_string());
                    RushError::new(RushErrorKind::EncodingError)
                })?;

                self.handle_command(line)
            })
            .last()
            .unwrap_or(Ok(0))
    }

    fn write_prompt(&self) -> Result<()> {
        let mut stdout = io::stdout().lock();
        stdout
            .write_all(self.ps1.as_bytes())
            .map_err(|_| RushError::new(RushErrorKind::IOError))?;

        stdout
            .flush()
            .map_err(|_| RushError::new(RushErrorKind::IOError))
    }

    fn read_line(&self) -> Result<String> {
        let mut buffer = String::new();
        let stdin = std::io::stdin();
        stdin.read_line(&mut buffer).map_err(|e| {
            if self.debug {
                eprintln!("Could not read from stdin: {e}");
            }
            RushError::new(RushErrorKind::IOError)
        })?;

        return Ok(buffer);
    }

    fn parse_command(&self, line: String) -> Result<Command> {
        let mut tokens = line.split_whitespace();
        let program = match tokens.next() {
            Some(p) => String::from(p),
            None => return Err(RushError::new(RushErrorKind::InvalidCommand)),
        };

        let args = tokens.map(String::from).collect::<Vec<String>>();

        Ok(Command { program, args })
    }

    fn handle_command(&self, line: String) -> Result<i32> {
        if self.debug {
            print!("{}", line.as_str());
        }

        let cmd = self.parse_command(line)?;

        match cmd.program.as_str().trim() {
            "exit" => {
                let code = cmd.args.get(0).and_then(|s| s.parse::<i32>().ok());
                crate::builtin::exit(code)
            }
            "cwd" => crate::builtin::cwd(),
            "cd" => {
                let path = Path::new(&cmd.args[0]);
                crate::builtin::cd(path)
            }
            "source" => {
                let script_path = Path::new(&cmd.args[0]);
                let fh = match std::fs::File::open(script_path) {
                    Ok(fh) => fh,
                    Err(e) => {
                        eprintln!("source: {}", e.to_string());
                        return Ok(-1);
                    }
                };

                let mut buf = BufReader::new(fh);

                return self.run_script(&mut buf);
            }
            _ => crate::builtin::exec(cmd),
        }
    }
}

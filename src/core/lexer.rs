use crate::core::char_stream::{CharStream, Position};
use crate::core::errors::*;
use crate::core::literal::Literal;
use crate::core::tokens::{Token, TokenInfo};
use std::io::BufRead;

pub type LexResult = Result<TokenInfo, LexError>;

#[derive(Debug)]
pub struct Lexer<'a, R>
where
    R: BufRead,
{
    token_it: CharStream<'a, R>,
    /// A boolean to make sure we emit a EOF when no more tokens can be extracted from the input
    /// program.
    eof_emitted: bool,
}

impl<'a, R> Lexer<'a, R>
where
    R: BufRead,
{
    pub fn new(reader: &'a mut R) -> Self {
        let token_it = CharStream::new(reader);

        Self {
            token_it,
            eof_emitted: false,
        }
    }

    /// Helper function to extract a num from the character iterator.
    fn emit_num_token(&mut self, first_char: char, start: Position) -> LexResult {
        let mut s = String::from(first_char);
        while self.token_it.peek().is_some_and(|c| c.is_digit(10)) {
            s.push(self.token_it.next().unwrap().0);
        }

        self.token_it.reset_peek();
        if self.token_it.peek().is_some_and(|c| c == '.') {
            if self.token_it.peek().is_some_and(|c| c.is_digit(10)) {
                s.push(self.token_it.next().unwrap().0);
                while self.token_it.peek().is_some_and(|c| c.is_digit(10)) {
                    s.push(self.token_it.next().unwrap().0);
                }
            }
        }

        self.token_it.reset_peek();
        let num = s.parse::<f64>().expect("parsing floating point from input");

        Ok(TokenInfo::new(Token::Literal(Literal::Num(num), s), start))
    }

    /// Helper function to extract a string from the character iterator.
    fn emit_string_token(&mut self, start: Position) -> LexResult {
        let mut s = String::from("\"");
        while let Some((c, _)) = self.token_it.next() {
            s.push(c);
            if c == '"' {
                let lexeme = s.clone();

                s.pop().expect("popping of quote from string literal");
                s.remove(0);

                return Ok(TokenInfo::new(
                    Token::Literal(Literal::Str(s.clone()), lexeme.clone()),
                    start,
                ));
            }
        }

        Err(LexError::new(LexErrorKind::UnterminatedString, start))
    }

    /// Helper function to extract an identifier from the character iterator.
    fn emit_ident_token(&mut self, first_char: char, start: Position) -> LexResult {
        let mut lexeme = String::from(first_char);
        while self
            .token_it
            .peek()
            .is_some_and(|c| c.is_ascii_alphanumeric() || c == '_')
        {
            lexeme.push(self.token_it.next().unwrap().0);
        }

        let tok = match lexeme.as_str() {
            "and" => Token::And,
            "class" => Token::Class,
            "else" => Token::Else,
            "for" => Token::For,
            "fun" => Token::Fun,
            "if" => Token::If,
            "or" => Token::Or,
            "return" => Token::Return,
            "super" => Token::Super,
            "this" => Token::This,
            "let" => Token::Let,
            "while" => Token::While,
            "false" => Token::Literal(Literal::False, lexeme.clone()),
            "nil" => Token::Literal(Literal::Nil, lexeme.clone()),
            "true" => Token::Literal(Literal::True, lexeme.clone()),
            _ => Token::Identifier(lexeme.clone()),
        };

        Ok(TokenInfo::new(tok, start))
    }

    fn emit_token_info(&self, kind: Token, pos: Position) -> Option<LexResult> {
        Some(Ok(TokenInfo::new(kind, pos)))
    }

    pub fn tokens(self) -> Self {
        self
    }

    pub fn yield_token(&mut self) -> Option<LexResult> {
        while let Some((character, start_pos)) = self.token_it.next() {
            match character {
                '(' => return self.emit_token_info(Token::LeftParen, start_pos),
                ')' => return self.emit_token_info(Token::RightParen, start_pos),
                '{' => return self.emit_token_info(Token::LeftBrace, start_pos),
                '}' => return self.emit_token_info(Token::RightBrace, start_pos),
                '*' => return self.emit_token_info(Token::Star, start_pos),
                '.' => return self.emit_token_info(Token::Dot, start_pos),
                ',' => return self.emit_token_info(Token::Comma, start_pos),
                '+' => return self.emit_token_info(Token::Plus, start_pos),
                '-' => return self.emit_token_info(Token::Minus, start_pos),
                ';' => return self.emit_token_info(Token::SemiColon, start_pos),
                '$' => return self.emit_token_info(Token::Dollar, start_pos),
                '=' => match self.token_it.peek() {
                    Some('=') => {
                        self.token_it.next();
                        return self.emit_token_info(Token::EqualEqual, start_pos);
                    }
                    _ => return self.emit_token_info(Token::Equal, start_pos),
                },
                '!' => match self.token_it.peek() {
                    Some('=') => {
                        self.token_it.next();
                        return self.emit_token_info(Token::BangEqual, start_pos);
                    }
                    _ => return self.emit_token_info(Token::Bang, start_pos),
                },
                '<' => match self.token_it.peek() {
                    Some('=') => {
                        self.token_it.next();
                        return self.emit_token_info(Token::LessEqual, start_pos);
                    }
                    _ => return self.emit_token_info(Token::Less, start_pos),
                },
                '>' => match self.token_it.peek() {
                    Some('=') => {
                        self.token_it.next();
                        return self.emit_token_info(Token::GreaterEqual, start_pos);
                    }
                    _ => return self.emit_token_info(Token::Greater, start_pos),
                },
                '/' => match self.token_it.peek() {
                    Some('/') => {
                        while self.token_it.peek().is_some_and(|c| c != '\n' && c != '\r') {
                            self.token_it.next();
                        }
                        continue;
                    }
                    _ => return self.emit_token_info(Token::Slash, start_pos),
                },
                '"' => return Some(self.emit_string_token(start_pos)),
                n @ '0'..='9' => return Some(self.emit_num_token(n, start_pos)),
                c if c.is_ascii_alphanumeric() || c == '_' => {
                    return Some(self.emit_ident_token(c, start_pos))
                }
                c if c.is_whitespace() => continue,
                _ => {
                    return Some(Err(LexError::new(
                        LexErrorKind::UnexpectedCharacter(character),
                        start_pos,
                    )));
                }
            };
        }
        if self.eof_emitted {
            None
        } else {
            if self.token_it.error().is_some() {
                return Some(Err(LexError::new(
                    LexErrorKind::EncodingError,
                    self.token_it.current_position(),
                )));
            }
            self.eof_emitted = true;
            self.emit_token_info(Token::EOF, self.token_it.current_position())
        }
    }
}

impl<'a, R> Iterator for Lexer<'a, R>
where
    R: BufRead,
{
    type Item = LexResult;
    fn next(&mut self) -> Option<Self::Item> {
        self.yield_token()
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use super::*;
    use crate::token;

    fn tokenize(program: &str) -> Vec<TokenInfo> {
        let mut bufread = Cursor::new(program);
        let lexer = Lexer::new(&mut bufread);

        lexer
            .collect::<Result<Vec<TokenInfo>, LexError>>()
            .expect("tokenizing string")
    }

    #[test]
    fn lex_simple_program() {
        let program = "1";
        let tokens = tokenize(program);

        let expected = vec![token!(Num, 1.0, "1", (1, 0)), token!(EOF, (1, 1))];

        assert_eq!(tokens, expected);
    }

    #[test]
    fn lex_arithmetics() {
        let program = "1 + 1";
        let tokens = tokenize(program);

        let expected = vec![
            token!(Num, 1.0, "1", (1, 0)),
            token!(Plus, (1, 2)),
            token!(Num, 1.0, "1", (1, 4)),
            token!(EOF, (1, 5)),
        ];

        assert_eq!(tokens, expected);
    }

    #[test]
    fn lex_floating_arithmetics() {
        let program = "1.0 + 1.1000010 + 10.1234";
        let tokens = tokenize(program);

        let expected = vec![
            token!(Num, 1.0, "1.0", (1, 0)),
            token!(Plus, (1, 4)),
            token!(Num, 1.100001, "1.1000010", (1, 6)),
            token!(Plus, (1, 16)),
            token!(Num, 10.1234, "10.1234", (1, 18)),
            token!(EOF, (1, 25)),
        ];

        assert_eq!(tokens, expected);
    }

    #[test]
    fn lex_long_program() {
        let program = r#"let x = 123;
        // This is a comment
        if ($x <= 10) {
            print("x is quite small");
        } else {
            x = $x + 123;
            // x is now much larger... "This should not be tokenized"
        }
        "#;
        let tokens = tokenize(program);
        let expected = vec![
            token!(Let, (1, 0)),
            token!(Identifier, "x", (1, 4)),
            token!(Equal, (1, 6)),
            token!(Num, 123.0, "123", (1, 8)),
            token!(SemiColon, (1, 11)),
            token!(If, (3, 8)),
            token!(LeftParen, (3, 11)),
            token!(Dollar, (3, 12)),
            token!(Identifier, "x", (3, 13)),
            token!(LessEqual, (3, 15)),
            token!(Num, 10.0, "10", (3, 18)),
            token!(RightParen, (3, 20)),
            token!(LeftBrace, (3, 22)),
            token!(Identifier, "print", (4, 12)),
            token!(LeftParen, (4, 17)),
            token!(Str, "x is quite small", "\"x is quite small\"", (4, 18)),
            token!(RightParen, (4, 36)),
            token!(SemiColon, (4, 37)),
            token!(RightBrace, (5, 8)),
            token!(Else, (5, 10)),
            token!(LeftBrace, (5, 15)),
            token!(Identifier, "x", (6, 12)),
            token!(Equal, (6, 14)),
            token!(Dollar, (6, 16)),
            token!(Identifier, "x", (6, 17)),
            token!(Plus, (6, 19)),
            token!(Num, 123.0, "123", (6, 21)),
            token!(SemiColon, (6, 24)),
            token!(RightBrace, (8, 8)),
            token!(EOF, (9, 8)),
        ];
        assert_eq!(tokens, expected);
    }
}

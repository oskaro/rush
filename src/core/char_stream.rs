pub use crate::core::position_annotation::Position;
use std::collections::VecDeque;
use utf8_chars::BufReadCharsExt;

use std::io::BufRead;
use utf8_chars::Chars;

#[derive(Debug)]
pub struct CharStream<'a, R>
where
    R: BufRead,
{
    inner: Chars<'a, R>,
    /// Buffer to implement peeking.
    buffer: VecDeque<char>,
    index: usize,
    stream_position: Position,
    error: Option<std::io::Error>,
}

impl<'a, R> Iterator for CharStream<'a, R>
where
    R: BufRead,
{
    type Item = (char, Position);
    fn next(&mut self) -> Option<Self::Item> {
        self.index = 0;
        if self.error().is_some() {
            return None;
        }
        let item = self
            .buffer
            .pop_front()
            .map(|e| Ok(e))
            .or_else(|| self.inner.next())?;
        // We need to return the current stream position with the current token.
        let stream_position = self.stream_position;
        let chr = match item {
            Ok(c) => {
                if c == '\n' {
                    self.stream_position = (self.stream_position.0 + 1, 0);
                } else {
                    self.stream_position.1 += 1;
                }
                c
            }
            Err(e) => {
                self.error = Some(e);
                return None;
            }
        };
        Some((chr, stream_position))
    }
}

impl<'a, R> CharStream<'a, R>
where
    R: BufRead,
{
    pub fn new(reader: &'a mut R) -> Self {
        let inner = reader.chars();
        Self {
            inner,
            buffer: VecDeque::new(),
            stream_position: (1, 0),
            index: 0,
            error: None,
        }
    }

    pub fn error(&self) -> &Option<std::io::Error> {
        &self.error
    }

    pub fn current_position(&self) -> (usize, usize) {
        self.stream_position
    }

    pub fn reset_peek(&mut self) {
        self.index = 0;
    }

    pub fn peek(&mut self) -> Option<char> {
        if self.error.is_some() {
            return None;
        }
        let ret = if self.index < self.buffer.len() {
            Some(self.buffer[self.index])
        } else {
            match self.inner.next() {
                Some(Ok(x)) => {
                    self.buffer.push_back(x);
                    Some(self.buffer[self.index])
                }
                Some(Err(e)) => {
                    self.error = Some(e);
                    return None;
                }
                None => return None,
            }
        };

        self.index += 1;
        ret
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use super::CharStream;

    #[test]
    fn peeking_works_as_expected() {
        let mut reader = Cursor::new("This is some long string");
        let mut stream = CharStream::new(&mut reader);

        assert_eq!(stream.peek(), Some('T'));
        assert_eq!(stream.peek(), Some('h'));
        assert_eq!(stream.peek(), Some('i'));
        assert_eq!(stream.peek(), Some('s'));
        stream.reset_peek();
        assert_eq!(stream.peek(), Some('T'));
        assert_eq!(stream.peek(), Some('h'));
        assert_eq!(stream.peek(), Some('i'));
        assert_eq!(stream.peek(), Some('s'));

        assert_eq!(stream.next(), Some(('T', (1, 0))));
        assert_eq!(stream.next(), Some(('h', (1, 1))));

        assert_eq!(stream.peek(), Some('i'));
        assert_eq!(stream.peek(), Some('s'));
        assert_eq!(stream.next(), Some(('i', (1, 2))));
    }

    #[test]
    fn errors_works_as_expected() {
        let bytes = &['T' as u8, ' ' as u8, 0xc3, 'i' as u8];
        let mut reader = Cursor::new(&bytes);
        let mut stream = CharStream::new(&mut reader);
        let mut chars = Vec::new();
        while let Some((c, p)) = stream.next() {
            chars.push((c, p));
        }

        assert_eq!(chars, vec![('T', (1, 0)), (' ', (1, 1))]);
        assert!(stream.error().is_some());
        assert!(stream.next().is_none());
    }
}

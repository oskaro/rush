use crate::core::interpreter::ErrorKind;
use crate::core::literal::Literal;
use std::ops::{Add, Div, Mul, Sub};

#[derive(Debug, PartialEq, Clone)]
pub enum BuiltIn {
    Str(String),
    Num(f64),
    True,
    False,
    Nil,
}

impl std::fmt::Display for BuiltIn {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Str(s) => s.fmt(f),
            Self::Num(n) => n.fmt(f),
            Self::True => true.fmt(f),
            Self::False => false.fmt(f),
            Self::Nil => write!(f, "nil"),
        }
    }
}

impl From<&Literal> for BuiltIn {
    fn from(value: &Literal) -> Self {
        match value {
            Literal::Str(s) => Self::Str(s.clone()),
            Literal::True => Self::True,
            Literal::Num(n) => Self::Num(*n),
            Literal::False => Self::False,
            Literal::Nil => Self::Nil,
        }
    }
}

impl From<&Literal> for Object {
    fn from(value: &Literal) -> Self {
        Self { kind: value.into() }
    }
}

impl From<f64> for Object {
    fn from(value: f64) -> Self {
        Self::new_num(value)
    }
}

impl From<String> for Object {
    fn from(value: String) -> Self {
        Self::new_str(value)
    }
}

impl From<bool> for Object {
    fn from(value: bool) -> Self {
        if value {
            Self::new_true()
        } else {
            Self::new_false()
        }
    }
}

impl TryInto<f64> for Object {
    type Error = ();

    fn try_into(self) -> Result<f64, Self::Error> {
        f64::try_from(&self)
    }
}
impl TryFrom<&Object> for f64 {
    type Error = ();
    fn try_from(value: &Object) -> Result<Self, Self::Error> {
        match value.kind {
            BuiltIn::Num(n) => Ok(n),
            _ => Err(()),
        }
    }
}

impl From<&Object> for String {
    fn from(value: &Object) -> String {
        match &value.kind {
            BuiltIn::Str(s) => s.clone(),
            BuiltIn::Nil => "".into(),
            BuiltIn::True => "true".into(),
            BuiltIn::False => "true".into(),
            BuiltIn::Num(n) => format!("{}", n),
        }
    }
}

impl From<&Object> for bool {
    fn from(value: &Object) -> bool {
        match &value.kind {
            BuiltIn::Str(s) if s.is_empty() => false,
            BuiltIn::Num(n) if *n == 0.0 => false,
            BuiltIn::Nil | BuiltIn::False => false,
            BuiltIn::Str(_) | BuiltIn::Num(_) | BuiltIn::True => true,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Object {
    pub(crate) kind: BuiltIn,
}

impl Add<Object> for Object {
    type Output = Result<Object, ErrorKind>;
    fn add(self, rhs: Object) -> Self::Output {
        match (self.kind, rhs.kind) {
            (BuiltIn::Str(l), BuiltIn::Str(r)) => Ok(Self::new_str(format!("{}{}", l, r))),
            (BuiltIn::Num(l), BuiltIn::Num(r)) => Ok(Self::new_num(l + r)),
            _ => Err(ErrorKind::OperandError(
                "Operands must be two numbers or two strings".into(),
            )),
        }
    }
}

impl Sub<Object> for Object {
    type Output = Result<Object, ErrorKind>;
    fn sub(self, rhs: Object) -> Self::Output {
        match (self.kind, rhs.kind) {
            (BuiltIn::Num(l), BuiltIn::Num(r)) => Ok(Self::new_num(l - r)),
            _ => Err(ErrorKind::OperandError("Operands must be numbers".into())),
        }
    }
}

impl Mul<Object> for Object {
    type Output = Result<Object, ErrorKind>;
    fn mul(self, rhs: Object) -> Self::Output {
        match (self.kind, rhs.kind) {
            (BuiltIn::Num(l), BuiltIn::Num(r)) => Ok(Self::new_num(l * r)),
            _ => Err(ErrorKind::OperandError("Operands must be numbers".into())),
        }
    }
}

impl Div<Object> for Object {
    type Output = Result<Object, ErrorKind>;
    fn div(self, rhs: Object) -> Self::Output {
        match (self.kind, rhs.kind) {
            (BuiltIn::Num(l), BuiltIn::Num(r)) => Ok(Self::new_num(l / r)),
            _ => Err(ErrorKind::OperandError("Operands must be numbers".into())),
        }
    }
}

impl Object {
    pub fn new(kind: BuiltIn) -> Self {
        Self { kind }
    }

    pub fn new_true() -> Self {
        Self::new(BuiltIn::True)
    }

    pub fn new_false() -> Self {
        Self::new(BuiltIn::False)
    }

    pub fn new_nil() -> Self {
        Self::new(BuiltIn::Nil)
    }

    pub fn new_num(value: f64) -> Self {
        Self::new(BuiltIn::Num(value))
    }

    pub fn new_str(value: String) -> Self {
        Self::new(BuiltIn::Str(value))
    }

    pub fn gt(&self, rhs: &Self) -> Result<Self, ErrorKind> {
        match (&self.kind, &rhs.kind) {
            (BuiltIn::Num(l), BuiltIn::Num(r)) => Ok((l > r).into()),
            _ => Err(ErrorKind::OperandError("Operands must be numbers".into())),
        }
    }
    pub fn gte(&self, rhs: &Self) -> Result<Self, ErrorKind> {
        match (&self.kind, &rhs.kind) {
            (BuiltIn::Num(l), BuiltIn::Num(r)) => Ok((l >= r).into()),
            _ => Err(ErrorKind::OperandError("Operands must be numbers".into())),
        }
    }
    pub fn lt(&self, rhs: &Self) -> Result<Self, ErrorKind> {
        let gte = self.gte(rhs)?;
        let lt: bool = !bool::from(&gte);

        Ok(lt.into())
    }

    pub fn lte(&self, rhs: &Self) -> Result<Self, ErrorKind> {
        let gt = self.gt(rhs)?;
        let lte: bool = !bool::from(&gt);

        Ok(lte.into())
    }
}

impl std::fmt::Display for Object {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.kind.fmt(f)
    }
}

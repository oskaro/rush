pub use crate::core::literal::Literal;
pub use crate::core::position_annotation::PositionAnnotation;
use std::fmt::Display;

/// A macro to make it easier to create tokens.
///
/// ```
///     #[macro_use] extern crate rush;
///     use rush::{token, core::tokens::{TokenInfo, Token}};
///     let token_info = token!(EOF, (0,0));
///     let expected_token_info = TokenInfo::new(Token::EOF, (0, 0));
///     assert_eq!(token_info, expected_token_info);
/// ```
///
/// Also it should be easier to make literals:
///
/// ```
///     #[macro_use] extern crate rush;
///     use rush::token;
///     use rush::core::tokens::{TokenInfo, Token, Literal};
///     let token_info = token!(Num, 3.0, "3", (0,0));
///     let expected_token_info = TokenInfo::new(Token::Literal(Literal::Num(3.0), "3".into()), (0, 0));
///     assert_eq!(token_info, expected_token_info);
///     let token_info = token!(Str, "Some str", "\"Some str\"", (0,0));
///     let expected_token_info = TokenInfo::new(Token::Literal(Literal::Str("Some str".into()), "\"Some str\"".into()), (0, 0));
///     assert_eq!(token_info, expected_token_info);
/// ```
#[macro_export]
macro_rules! token {
    ($tok:ident, $pos:expr) => {{
        use $crate::core::position_annotation::PositionAnnotation;
        use $crate::core::tokens::*;
        PositionAnnotation::new(Token::$tok, $pos)
    }};
    ($tok:ident, $val:expr, $pos:expr) => {{
        use $crate::core::position_annotation::PositionAnnotation;
        use $crate::core::tokens::*;
        PositionAnnotation::new(Token::$tok($val.into()), $pos)
    }};
    ($tok:ident, $val:expr, $lexeme:expr, $pos:expr) => {{
        use $crate::core::position_annotation::PositionAnnotation;
        use $crate::core::tokens::*;
        PositionAnnotation::new(
            Token::Literal(Literal::$tok($val.into()), $lexeme.into()),
            $pos,
        )
    }};
}

pub type TokenInfo = PositionAnnotation<Token>;

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    EOF,
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Star,
    Dot,
    Comma,
    Plus,
    Minus,
    SemiColon,
    Equal,
    EqualEqual,
    Bang,
    BangEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    Slash,
    Literal(Literal, String),
    Identifier(String),
    And,
    Class,
    Else,
    For,
    Fun,
    If,
    Or,
    Return,
    Super,
    This,
    Let,
    While,
    Dollar,
}

impl Token {
    pub fn name(&self) -> &'static str {
        match self {
            Token::EOF => "EOF",
            Token::LeftParen => "LEFT_PAREN",
            Token::RightParen => "RIGHT_PAREN",
            Token::LeftBrace => "LEFT_BRACE",
            Token::RightBrace => "RIGHT_BRACE",
            Token::Star => "STAR",
            Token::Dot => "DOT",
            Token::Comma => "COMMA",
            Token::Plus => "PLUS",
            Token::Minus => "MINUS",
            Token::SemiColon => "SEMICOLON",
            Token::Equal => "EQUAL",
            Token::EqualEqual => "EQUAL_EQUAL",
            Token::Bang => "BANG",
            Token::BangEqual => "BANG_EQUAL",
            Token::Less => "LESS",
            Token::LessEqual => "LESS_EQUAL",
            Token::Greater => "GREATER",
            Token::GreaterEqual => "GREATER_EQUAL",
            Token::Slash => "SLASH",
            Token::Literal(Literal::Str(_), _) => "STRING",
            Token::Literal(Literal::Num(_), _) => "NUMBER",
            Token::Literal(Literal::Nil, _) => "NIL",
            Token::Literal(Literal::True, _) => "TRUE",
            Token::Literal(Literal::False, _) => "FALSE",
            Token::Identifier(_) => "IDENTIFIER",
            Token::And => "AND",
            Token::Class => "CLASS",
            Token::Else => "ELSE",
            Token::For => "FOR",
            Token::Fun => "FUN",
            Token::If => "IF",
            Token::Or => "OR",
            Token::Return => "RETURN",
            Token::Super => "SUPER",
            Token::This => "THIS",
            Token::Let => "LET",
            Token::While => "WHILE",
            Token::Dollar => "DOLLAR",
        }
    }

    pub fn lexeme(&self) -> &str {
        match &self {
            Token::EOF => "".into(),
            Token::LeftParen => "(".into(),
            Token::RightParen => ")".into(),
            Token::LeftBrace => "{".into(),
            Token::RightBrace => "}".into(),
            Token::Star => "*".into(),
            Token::Dot => ".".into(),
            Token::Comma => ",".into(),
            Token::Plus => "+".into(),
            Token::Minus => "-".into(),
            Token::SemiColon => ";".into(),
            Token::Equal => "=".into(),
            Token::EqualEqual => "==".into(),
            Token::Bang => "!".into(),
            Token::BangEqual => "!=".into(),
            Token::Less => "<".into(),
            Token::LessEqual => "<=".into(),
            Token::Greater => ">".into(),
            Token::GreaterEqual => ">=".into(),
            Token::Slash => "/".into(),
            Token::And => "and".into(),
            Token::Class => "class".into(),
            Token::Else => "else".into(),
            Token::For => "for".into(),
            Token::Fun => "fun".into(),
            Token::If => "if".into(),
            Token::Or => "or".into(),
            Token::Return => "return".into(),
            Token::Super => "super".into(),
            Token::This => "this".into(),
            Token::Let => "let".into(),
            Token::While => "while".into(),
            Token::Dollar => "$".into(),
            Token::Identifier(s) => s,
            Token::Literal(Literal::False, _) => "false".into(),
            Token::Literal(Literal::True, _) => "true".into(),
            Token::Literal(Literal::Nil, _) => "nil".into(),
            Token::Literal(Literal::Str(_), s) => s,
            Token::Literal(Literal::Num(_), s) => s,
        }
    }

    pub fn value(&self) -> String {
        match &self {
            Token::Literal(Literal::Str(s), _) => s.into(),
            Token::Literal(Literal::Num(n), _) => format!("{n:?}"),
            _ => "null".into(),
        }
    }

    pub fn kind(&self) -> &Token {
        &self
    }

    pub fn is_kind(&self, kind: &Token) -> bool {
        std::mem::discriminant(kind) == std::mem::discriminant(&self)
    }

    pub fn is_any_of(&self, kinds: &[Token]) -> bool {
        kinds.iter().any(|k| self.is_kind(k))
    }
}

impl Display for TokenInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} '{}' '{}'", self.name(), self.lexeme(), self.value())
    }
}

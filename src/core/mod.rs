pub mod ast;
pub mod char_stream;
pub mod errors;
pub mod expressions;
pub mod interpreter;
pub mod lexer;
pub mod literal;
pub mod objects;
pub mod parser;
pub mod position_annotation;
pub mod session;
pub mod statements;
pub mod tokens;

pub use errors::RushError;
pub use errors::RushErrorKind;
pub type Result<T> = std::result::Result<T, RushError>;
pub use interpreter::Interpreter;
pub use lexer::Lexer;
pub use objects::Object;
pub use parser::Parser;

pub(crate) struct Command {
    pub program: String,
    pub args: Vec<String>,
}

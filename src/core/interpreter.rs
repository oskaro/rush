use std::cell::RefCell;
use std::collections::HashMap;
use std::ops::Deref;
use std::process;
use tracing::{event, Level};

use crate::core::ast::AST;
use crate::core::expressions::Expr;
use crate::core::objects::*;
use crate::core::tokens::Token;

use super::position_annotation::Position;
use super::statements::Stmt;
use super::{Lexer, Parser, RushError};

pub trait Visitor<T> {
    fn visit(&self, e: &Expr) -> T;
}

#[derive(Debug, Clone)]
pub enum ErrorKind {
    OperandError(String),
    UndefinedIdentifier(String),
    OsError,
    UnknownError,
}

#[derive(Debug, Clone)]
pub struct Error {
    kind: ErrorKind,
    position: Position,
}

impl Error {
    pub fn new(kind: ErrorKind, position: (usize, usize)) -> Self {
        Self { kind, position }
    }

    pub fn position(&self) -> Position {
        self.position
    }

    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{},{}: ", self.position.0, self.position.1)?;
        match &self.kind {
            ErrorKind::OperandError(msg) => write!(f, "{msg}"),
            ErrorKind::UnknownError => write!(f, "Unknown error"),
            ErrorKind::UndefinedIdentifier(ident) => {
                write!(f, "Identifier '{ident}' is not defined.")
            }
            ErrorKind::OsError => {
                write!(f, "OS Error")
            }
        }
    }
}

pub type Result<T> = std::result::Result<T, RushError>;

#[derive(Debug, Clone)]
pub struct Environment {
    inner: RefCell<HashMap<String, Object>>,
    parent: Option<Box<RefCell<Environment>>>,
}

impl Environment {
    pub fn new() -> Self {
        Self {
            inner: RefCell::new(HashMap::new()),
            parent: None,
        }
    }

    pub fn new_scope(parent: &RefCell<Self>) -> Self {
        Self {
            inner: RefCell::new(HashMap::new()),
            parent: Some(Box::new(parent.clone())),
        }
    }

    pub fn insert(&self, key: String, object: Object) -> Option<Object> {
        event!(Level::TRACE, ?key, ?object, "declaration");
        self.inner.borrow_mut().insert(key, object)
    }

    pub fn get(&self, key: &str) -> Option<Object> {
        event!(Level::TRACE, ?key, "evaluation");
        self.inner.borrow().get(key).map(|o| o.clone()).or_else(|| {
            if let Some(ref scoped) = self.parent {
                event!(Level::TRACE, ?key, "evaluating parent scope");
                scoped.borrow().get(key)
            } else {
                None
            }
        })
    }
}

pub struct Interpreter {
    ast: AST,
    global_env: RefCell<Environment>,
}

impl Interpreter {
    pub fn new(ast: AST) -> Self {
        Self {
            ast,
            global_env: RefCell::new(Environment::new()),
        }
    }

    pub fn from_reader<R: std::io::BufRead>(
        reader: &mut R,
    ) -> std::result::Result<Self, RushError> {
        let lexer = Lexer::new(reader);
        let parser = Parser::with_lexer(lexer);
        let ast = parser.parse()?;

        Ok(Self {
            ast,
            global_env: RefCell::new(Environment::new()),
        })
    }

    pub fn evaluate(self) -> Vec<Result<Option<Object>>> {
        return Self::visit_stmts(self.ast.iter().map(|stmt| stmt), &self.global_env);
    }

    fn visit_stmts<'a>(
        stmts: impl Iterator<Item = &'a Stmt>,
        env: &RefCell<Environment>,
    ) -> Vec<Result<Option<Object>>> {
        stmts.map(|stmt| Self::visit_stmt(&stmt, env)).collect()
    }

    fn visit_stmt(s: &Stmt, env: &RefCell<Environment>) -> Result<Option<Object>> {
        match s {
            Stmt::Expr(e) => Self::visit_expr(e, env).map(|o| Some(o)),
            Stmt::Decl {
                ident,
                initializer: Some(initializer),
            } => {
                let initializer = Self::visit_expr(initializer, env)?;
                env.borrow_mut().insert(ident.clone(), initializer);
                Ok(None)
            }
            Stmt::Decl {
                ident,
                initializer: None,
            } => {
                env.borrow_mut().insert(ident.clone(), Object::new_nil());
                Ok(None)
            }
            Stmt::Block(stmts) => {
                let scope = RefCell::new(Environment::new_scope(env));
                Self::visit_block(stmts.iter().map(|i| i.as_ref()), &scope)
            }
        }
    }

    pub fn visit_block<'a>(
        stmts: impl Iterator<Item = &'a Stmt>,
        env: &RefCell<Environment>,
    ) -> Result<Option<Object>> {
        let val = Self::visit_stmts(stmts, env)
            .into_iter()
            .collect::<Result<Vec<Option<Object>>>>()?;
        match val.last() {
            Some(obj) => Ok(obj.clone()),
            None => Ok(None),
        }
    }

    pub fn call_subcommand(program: &str, args: &[Object]) -> Result<Object> {
        let args: Vec<String> = args.iter().map(|a| String::from(a)).collect();
        match process::Command::new(program)
            .args(&args)
            .spawn()
            .unwrap()
            .wait()
        {
            Ok(ec) => Ok(Object::new_num(ec.code().unwrap_or(0).into())),
            Err(e) => Err(e.into()),
        }
    }

    pub fn visit_expr(e: &Expr, env: &RefCell<Environment>) -> Result<Object> {
        let res = match e {
            Expr::Call { command, args } => {
                let args: Vec<Object> = args
                    .iter()
                    .map(|e| Self::visit_expr(e, env))
                    .collect::<Result<Vec<Object>>>()?;
                Self::call_subcommand(command, &args[..])?
            }
            Expr::Identifier(i) => match env.borrow().get(i) {
                Some(o) => o.clone(),
                None => {
                    return Err(Error {
                        kind: ErrorKind::UndefinedIdentifier(i.clone()),
                        position: (0, 0),
                    }
                    .into())
                }
            },
            Expr::Expansion(e) => {
                if let Some(val) = env.borrow().get(e) {
                    val.clone()
                } else {
                    Object::new_nil()
                }
            }
            Expr::Assign { ident, expr } => {
                let expr = Self::visit_expr(expr, env)?;
                match env.borrow_mut().insert(ident.clone(), expr.clone()) {
                    Some(_) => expr,
                    None => {
                        return Err(Error::new(
                            ErrorKind::UndefinedIdentifier(ident.clone()),
                            (0, 0),
                        )
                        .into())
                    }
                }
            }
            Expr::Literal(l) => Object::from(l.deref()),
            Expr::Grouping(e) => Self::visit_expr(e, env)?,
            Expr::Unary { operator, operand } => {
                let operand = Self::visit_expr(operand, env)?;
                match (operand.kind, operator.kind()) {
                    (BuiltIn::Num(n), Token::Minus) => Object::new_num(-n),
                    (_, Token::Minus) => {
                        return Err(Error::new(
                            ErrorKind::OperandError("Operand must be a number".into()),
                            operator.position(),
                        )
                        .into())
                    }
                    (BuiltIn::True, Token::Bang) => Object::new_false(),
                    (BuiltIn::False, Token::Bang) => Object::new_true(),
                    (BuiltIn::Nil, Token::Bang) => Object::new_true(),
                    (BuiltIn::Str(s), Token::Bang) if s == "" => Object::new_true(),
                    (BuiltIn::Str(_), Token::Bang) => Object::new_false(),
                    (BuiltIn::Num(0.0), Token::Bang) => Object::new_true(),
                    (BuiltIn::Num(_), Token::Bang) => Object::new_false(),
                    _ => {
                        return Err(Error::new(ErrorKind::UnknownError, operator.position()).into())
                    }
                }
            }
            Expr::Binary {
                left,
                right,
                operator,
            } => {
                let left = Self::visit_expr(left, env)?;
                let right = Self::visit_expr(right, env)?;
                let result = match operator.kind() {
                    Token::Plus => left + right,
                    Token::Minus => left - right,
                    Token::Star => left * right,
                    Token::Slash => left / right,
                    Token::Less => left.lt(&right),
                    Token::LessEqual => left.lte(&right),
                    Token::Greater => left.gt(&right),
                    Token::GreaterEqual => left.gte(&right),
                    Token::EqualEqual => Ok((left == right).into()),
                    Token::BangEqual => Ok((left != right).into()),

                    _ => panic!(),
                };
                result.or_else(|e| Err(Error::new(e, operator.position()).into()))?
            }
        };
        Ok(res)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::core::literal::{Literal, LiteralInfo};
    use crate::core::tokens::TokenInfo;
    #[test]
    fn test_that_visiting_as_string_works_correct() {
        let ast = Stmt::Expr(Expr::Binary {
            left: Box::new(Expr::Literal(LiteralInfo::new(Literal::Num(2.0), (1, 0)))),
            right: Box::new(Expr::Literal(LiteralInfo::new(Literal::Num(2.0), (1, 4)))),
            operator: TokenInfo::new(Token::Plus, (1, 3)),
        });

        let i = Interpreter::new(AST(vec![ast]));

        let res = i.evaluate();

        let res = res
            .into_iter()
            .next()
            .expect("getting first statement from interpreter");
        let res = res.expect("interpreter should produce valid results");
        let res = res.as_ref().expect("the result should be an object");
        let res: f64 = res.try_into().expect("the object should be a number");

        assert_eq!(res, 4.0f64);
    }

    #[test]
    fn test_that_we_can_output_string_comparisons() {
        let ast = AST(vec![Stmt::Expr(Expr::Binary {
            left: Box::new(Expr::Literal(LiteralInfo::new(
                Literal::Str("world".into()),
                (1, 0),
            ))),
            right: Box::new(Expr::Literal(LiteralInfo::new(
                Literal::Str("baz".into()),
                (1, 6),
            ))),
            operator: TokenInfo::new(Token::BangEqual, (1, 9)),
        })]);

        let i = Interpreter::new(ast);
        let res = i.evaluate();

        let res = res
            .into_iter()
            .next()
            .expect("getting first statement from interpreter");
        let res = res.expect("interpreter should produce valid results");
        let res = res.as_ref().expect("the result should be an object");
        let res: bool = res.try_into().expect("the object should be a boolean");

        assert!(res);
    }
}

use std::{fmt::Display, ops::Index};

use super::statements::Stmt;

#[derive(Debug, PartialEq, Clone)]
pub struct AST(pub(crate) Vec<Stmt>);

impl Index<usize> for AST {
    type Output = Stmt;
    fn index(&self, index: usize) -> &Self::Output {
        self.0.index(index)
    }
}

type ASTIter<'a> = std::slice::Iter<'a, Stmt>;
type ASTMutIter<'a> = std::slice::IterMut<'a, Stmt>;

impl AST {
    pub fn iter(&self) -> ASTIter {
        self.0.iter()
    }

    pub fn iter_mut(&mut self) -> ASTMutIter {
        self.0.iter_mut()
    }

    pub fn into_iter(self) -> std::vec::IntoIter<Stmt> {
        self.0.into_iter()
    }
}

impl Display for AST {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "AST {{")?;
        for expr in self.0.iter() {
            writeln!(f, "  {},", expr)?;
        }
        write!(f, "}}")
    }
}

use crate::core::literal::LiteralInfo;
use crate::core::tokens::TokenInfo;

#[derive(Debug, PartialEq, Clone)]
pub enum Expr {
    Binary {
        left: Box<Expr>,
        right: Box<Expr>,
        operator: TokenInfo,
    },
    Grouping(Box<Expr>),
    Literal(LiteralInfo),
    Identifier(String),
    Expansion(String),
    Call {
        command: String,
        args: Vec<Box<Expr>>,
    },
    Assign {
        ident: String,
        expr: Box<Expr>,
    },
    Unary {
        operator: TokenInfo,
        operand: Box<Expr>,
    },
}

impl std::fmt::Display for Expr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Binary {
                left,
                right,
                operator,
            } => write!(f, "({} {} {})", operator.lexeme(), left, right),
            Self::Unary { operator, operand } => {
                write!(f, "({} {})", operator.lexeme(), operand)
            }
            Self::Literal(l) => l.fmt(f),
            Self::Expansion(c) => write!(f, "(exp {})", c),
            Self::Grouping(e) => write!(f, "(group {})", e),
            Self::Identifier(i) => write!(f, "(ident {})", i),
            Self::Assign { ident, expr } => write!(f, "(assign {} {})", ident, expr),
            Self::Call { command, args } => write!(f, "(call {} {:?})", command, args),
        }
    }
}

use crate::core::ast::AST;
use crate::core::expressions::Expr;
use std::fmt::Display;
use std::io::BufRead;
use tracing::{event, instrument, Level};

use crate::core::literal::LiteralInfo;
use crate::core::tokens::{Token, TokenInfo};

use super::statements::Stmt;
use super::{Lexer, RushError};

#[derive(Debug, Clone)]
pub enum SyntaxErrorKind {
    UnmatchedParentheses,
    UnrecognizedToken(TokenInfo),
    UnexpectedEOF,
    MissingSemicolon,
    ExpectedIdentifier,
}

#[derive(Debug, Clone)]
pub struct SyntaxError {
    kind: SyntaxErrorKind,
}

impl Display for SyntaxError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.kind {
            SyntaxErrorKind::UnmatchedParentheses => write!(f, "Unmatched parentheses."),
            SyntaxErrorKind::UnexpectedEOF => write!(f, "Unexpected EOF."),
            SyntaxErrorKind::UnrecognizedToken(t) => write!(f, "Unrecognized token: {:?}.", t),
            SyntaxErrorKind::MissingSemicolon => write!(f, "Expected ';' after expression."),
            SyntaxErrorKind::ExpectedIdentifier => write!(f, "Expected identifier after 'let'."),
        }
    }
}
type Result<T> = std::result::Result<T, RushError>;

pub struct Parser<'a, R>
where
    R: BufRead,
{
    lexer: Lexer<'a, R>,
    token_buffer: Vec<TokenInfo>,
    ast: AST,
    loc: usize,
}
impl<'a, R> std::fmt::Debug for Parser<'a, R>
where
    R: BufRead,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Parser")
            .field("token_buffer", &self.token_buffer)
            .field("ast", &self.ast)
            .field("loc", &self.loc)
            .finish()
    }
}

impl<'a, R> Parser<'a, R>
where
    R: BufRead,
{
    pub fn with_lexer(lexer: Lexer<'a, R>) -> Self {
        let tokens = lexer.tokens();
        Self::new(tokens)
    }

    pub fn new(lexer: Lexer<'a, R>) -> Self {
        Self {
            lexer,
            token_buffer: Vec::new(),
            ast: AST(Vec::new()),
            loc: 0,
        }
    }

    pub fn parse(mut self) -> Result<AST> {
        while !self.at_end() {
            let expr = self.declaration()?;
            self.ast.0.push(expr);
        }
        Ok(self.ast)
    }

    fn at_end(&mut self) -> bool {
        if self.loc < self.token_buffer.len() {
            self.token_buffer
                .get(self.loc)
                .is_some_and(|t| t.is_kind(&Token::EOF))
        } else {
            if let Some(next) = self.next() {
                match next {
                    Ok(tok) => return tok.is_kind(&Token::EOF),
                    Err(_) => return true,
                }
            }
            false
        }
    }

    fn next(&mut self) -> Option<Result<&TokenInfo>> {
        if self.loc >= self.token_buffer.len() {
            let item = self.lexer.next()?;
            match item {
                Ok(item) => {
                    self.token_buffer.push(item);
                }
                Err(e) => return Some(Err(e.into())),
            }
        }

        self.token_buffer.get(self.loc).map(|i| Ok(i))
    }

    #[instrument(skip(self))]
    fn consume(&mut self, kind: Token, err: RushError) -> Result<()> {
        if let Some(next) = self.next() {
            let token = next?;
            if token.is_kind(&kind) {
                event!(Level::TRACE, "found token to consume");
                self.advance();
                return Ok(());
            } else {
                return Err(err);
            }
        } else {
            return Err(RushError::new(super::RushErrorKind::ParseError(
                SyntaxError {
                    kind: SyntaxErrorKind::UnexpectedEOF,
                },
            )));
        }
    }

    fn prev(&self) -> Option<&TokenInfo> {
        self.token_buffer.get(self.loc - 1)
    }

    fn advance(&mut self) -> usize {
        self.loc += 1;
        self.loc
    }

    fn matching(&mut self, kind: &Token) -> bool {
        if let Some(next) = self.next() {
            if next.is_ok_and(|t| t.is_kind(kind)) {
                self.advance();
                return true;
            }
        }
        false
    }

    #[instrument(skip(self))]
    fn declaration(&mut self) -> Result<Stmt> {
        if self.matching(&Token::Let) {
            event!(Level::TRACE, "Found a let statement");
            self.variable_declaration()
        } else {
            event!(Level::TRACE, "Found a regular statement");
            self.statement()
        }
    }

    #[instrument(skip(self))]
    fn variable_declaration(&mut self) -> Result<Stmt> {
        self.consume(
            Token::Identifier(String::new()),
            RushError::new(super::RushErrorKind::ParseError(SyntaxError {
                kind: SyntaxErrorKind::ExpectedIdentifier,
            })),
        )?;

        let ident = self.prev().unwrap().lexeme().to_string();
        event!(Level::TRACE, ?ident, "consumed an identifier");
        let initializer = if self.matching(&Token::Equal) {
            event!(Level::TRACE, "expecting an initializer");
            Some(self.expression()?)
        } else {
            None
        };

        event!(Level::TRACE, "consuming a SemiColon after expression");
        self.consume(
            Token::SemiColon,
            SyntaxError {
                kind: SyntaxErrorKind::MissingSemicolon,
            }
            .into(),
        )?;
        event!(Level::TRACE, "emitting a declaration");
        Ok(Stmt::Decl { ident, initializer })
    }

    #[instrument(skip(self))]
    fn statement(&mut self) -> Result<Stmt> {
        if self.matching(&Token::LeftBrace) {
            return self.block();
        }
        self.expression_statement()
    }

    #[instrument(skip(self))]
    fn block(&mut self) -> Result<Stmt> {
        let mut stmts = Vec::new();
        while !self.matching(&Token::RightBrace) {
            stmts.push(Box::new(self.declaration()?));
        }

        Ok(Stmt::Block(stmts))
    }

    #[instrument(skip(self))]
    fn expression_statement(&mut self) -> Result<Stmt> {
        let expr = self.expression()?;

        if !self
            .next()
            .is_some_and(|t| t.is_ok_and(|t| t.is_kind(&Token::Identifier(String::new()))))
        {
        }

        event!(Level::TRACE, "consuming a SemiColon after expression");
        self.consume(
            Token::SemiColon,
            SyntaxError {
                kind: SyntaxErrorKind::MissingSemicolon,
            }
            .into(),
        )?;
        return Ok(Stmt::Expr(expr));
    }

    #[instrument(skip(self))]
    fn expression(&mut self) -> Result<Expr> {
        let mut expr = self.assignment()?;
        event!(Level::TRACE, ?expr, "Got assignment");
        if let Expr::Identifier(s) = expr {
            event!(Level::TRACE, "Found command");
            let command = s;
            let mut args = Vec::new();

            loop {
                if self
                    .next()
                    .is_some_and(|t| t.is_ok_and(|t| t.is_kind(&Token::SemiColon)))
                {
                    break;
                }
                let arg = self.equality()?;
                event!(Level::TRACE, ?arg, ?self.loc, "found argument");
                args.push(Box::new(arg));
            }
            expr = Expr::Call { command, args };
        }
        Ok(expr)
    }

    #[instrument(skip(self))]
    fn assignment(&mut self) -> Result<Expr> {
        let mut expr = self.equality()?;
        event!(Level::TRACE, ?expr, "Trying to parse assignment");
        if let Some(next) = self.next() {
            let token = next?;
            if token.is_kind(&Token::Equal) {
                event!(Level::TRACE, ?expr, "found assignment");
                self.advance();
                let value = self.assignment()?;

                expr = match expr {
                    Expr::Expansion(s) | Expr::Identifier(s) => Expr::Assign {
                        ident: s,
                        expr: Box::new(value),
                    },
                    _ => {
                        return Err(RushError::new(super::RushErrorKind::ParseError(
                            SyntaxError {
                                kind: SyntaxErrorKind::ExpectedIdentifier,
                            },
                        )))
                    }
                };
            }
        }

        Ok(expr)
    }

    #[instrument(skip(self))]
    fn equality(&mut self) -> Result<Expr> {
        let mut expr = self.comparison()?;
        loop {
            if let Some(next) = self.next() {
                let t = next.map_err(|e| e.into())?;
                if t.is_any_of(&[Token::EqualEqual, Token::BangEqual]) {
                    self.advance();
                } else {
                    break;
                }
            }
            let operator = self.prev().expect("getting operator for equality").clone();
            let right = self.comparison()?;
            expr = Expr::Binary {
                left: Box::new(expr),
                right: Box::new(right),
                operator,
            };
        }

        Ok(expr)
    }

    #[instrument(skip(self))]
    fn comparison(&mut self) -> Result<Expr> {
        let mut expr = self.term()?;

        loop {
            if let Some(t) = self.next() {
                let t = t.map_err(|e| e.into())?;
                if t.is_any_of(&[
                    Token::Greater,
                    Token::GreaterEqual,
                    Token::Less,
                    Token::LessEqual,
                ]) {
                    self.advance();
                } else {
                    break;
                }
            }

            let operator = self
                .prev()
                .expect("getting operator for comparison")
                .clone();
            let right = Box::new(self.term()?);
            expr = Expr::Binary {
                left: Box::new(expr),
                right,
                operator,
            }
        }
        Ok(expr)
    }

    #[instrument(skip(self))]
    fn term(&mut self) -> Result<Expr> {
        let mut expr = self.factor()?;

        loop {
            if let Some(next) = self.next() {
                let t = next.map_err(|e| e.into())?;
                if t.is_any_of(&[Token::Plus, Token::Minus]) {
                    self.advance();
                } else {
                    break;
                }
            }
            let operator = self.prev().expect("getting operator for term").clone();
            let right = Box::new(self.factor()?);
            expr = Expr::Binary {
                left: Box::new(expr),
                right,
                operator,
            }
        }

        Ok(expr)
    }

    #[instrument(skip(self))]
    fn factor(&mut self) -> Result<Expr> {
        let mut expr = self.unary()?;

        loop {
            if let Some(next) = self.next() {
                let token = next.map_err(|e| e.into())?;
                if token.is_any_of(&[Token::Star, Token::Slash]) {
                    self.advance();
                } else {
                    break;
                }
            }
            let operator = self.prev().expect("getting factor operator").clone();
            let right = Box::new(self.unary()?);
            expr = Expr::Binary {
                left: Box::new(expr),
                right,
                operator,
            }
        }

        Ok(expr)
    }

    #[instrument(skip(self))]
    fn unary(&mut self) -> Result<Expr> {
        if let Some(next) = self.next() {
            let token = next.map_err(|e| e.into())?;
            if token.is_any_of(&[Token::Bang, Token::Minus]) {
                self.advance();

                let operator = self.prev().expect("getting unary operator").clone();
                let operand = Box::new(self.unary()?);

                Ok(Expr::Unary { operator, operand })
            } else {
                event!(Level::TRACE, "looking for a primary");
                self.primary()
            }
        } else {
            Err(RushError::new(super::RushErrorKind::ParseError(
                SyntaxError {
                    kind: SyntaxErrorKind::UnexpectedEOF,
                },
            )))
        }
    }

    #[instrument(skip(self))]
    fn primary(&mut self) -> Result<Expr> {
        if let Some(next) = self.next() {
            let token = next.map_err(|e| e.into())?.clone();
            let expr = match token.kind() {
                Token::Literal(l, _) => {
                    event!(Level::TRACE, "found literal");
                    self.advance();
                    Expr::Literal(LiteralInfo::new(l.clone(), token.position()))
                }
                Token::Dollar => {
                    self.advance();
                    let next = self.next();
                    event!(Level::TRACE, ?next, "found dollar sign");
                    let expr = match next {
                        Some(Ok(at)) if at.is_kind(&Token::Identifier(String::new())) => {
                            Expr::Expansion(at.lexeme().into())
                        }
                        Some(Err(e)) => return Err(e.into()),
                        _ => {
                            return Err(SyntaxError {
                                kind: SyntaxErrorKind::ExpectedIdentifier,
                            }
                            .into())
                        }
                    };
                    self.advance();
                    expr
                }
                Token::Identifier(i) => {
                    event!(Level::TRACE, "found identifier");
                    self.advance();
                    // TODO this feels like the wrong way to do it. Because if we do something
                    // like:
                    //  echo = "something"
                    // echo is still an identifier, not a command.
                    Expr::Identifier(i.clone())
                }
                Token::LeftParen => {
                    event!(Level::TRACE, "found left parentheses");
                    self.advance();
                    let expr = self.expression()?;

                    self.consume(
                        Token::RightParen,
                        SyntaxError {
                            kind: SyntaxErrorKind::UnmatchedParentheses,
                        }
                        .into(),
                    )?;

                    Expr::Grouping(Box::new(expr))
                }
                Token::EOF => {
                    event!(Level::TRACE, "found EOF");
                    return Err(SyntaxError {
                        kind: SyntaxErrorKind::UnexpectedEOF,
                    }
                    .into());
                }
                t @ _ => {
                    let prev = self.prev();
                    event!(Level::ERROR, ?t, ?prev, "Unexpected token");
                    return Err(SyntaxError {
                        kind: SyntaxErrorKind::UnrecognizedToken(token),
                    }
                    .into());
                }
            };
            event!(Level::INFO, ?expr, ?self.loc, "emitting expression");
            return Ok(expr);
        }
        Err(SyntaxError {
            kind: SyntaxErrorKind::UnexpectedEOF,
        }
        .into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::core::literal::Literal;
    use std::io::Cursor;

    fn parse_program<'a>(program: &'a str) -> Result<AST> {
        let mut reader = Cursor::new(program);
        let lexer = Lexer::new(&mut reader);
        let parser = Parser::with_lexer(lexer);

        parser.parse()
    }

    #[test]
    fn test_that_we_can_construct_parser() {
        let ast = parse_program("").expect("parsing empty token set");
        assert_eq!(ast, AST(Vec::new()));
    }

    #[test]
    fn test_that_we_can_parse_literal_expressions() {
        let ast = parse_program("1;").expect("parsing empty token set");
        assert_ne!(ast, AST(Vec::new()));
    }

    #[test]
    fn test_that_we_can_parse_a_simple_addition() {
        let program = "2.0 + 2.0;";
        let ast = parse_program(program).expect("parsing valid program");
        assert_eq!(
            ast,
            AST(vec![Stmt::Expr(Expr::Binary {
                left: Box::new(Expr::Literal(LiteralInfo::new(Literal::Num(2.0), (1, 0)))),
                right: Box::new(Expr::Literal(LiteralInfo::new(Literal::Num(2.0), (1, 6)))),
                operator: TokenInfo::new(Token::Plus, (1, 4))
            })])
        )
    }

    #[test]
    fn test_that_we_can_parse_a_group() {
        let program = "(2 + 2);";
        let ast = parse_program(program).expect("parsing valid program");
        assert_eq!(
            ast,
            AST(vec![Stmt::Expr(Expr::Grouping(Box::new(Expr::Binary {
                left: Box::new(Expr::Literal(LiteralInfo::new(Literal::Num(2.0), (1, 1)))),
                right: Box::new(Expr::Literal(LiteralInfo::new(Literal::Num(2.0), (1, 5)))),
                operator: TokenInfo::new(Token::Plus, (1, 3))
            })))])
        )
    }

    #[test]
    fn test_that_we_can_parse_string_comparison() {
        let program = r#""world" != "baz";"#;
        let ast = parse_program(program).expect("parsing valid program");
        assert_eq!(
            ast,
            AST(vec![Stmt::Expr(Expr::Binary {
                left: Box::new(Expr::Literal(LiteralInfo::new(
                    Literal::Str("world".into()),
                    (1, 0)
                ))),
                right: Box::new(Expr::Literal(LiteralInfo::new(
                    Literal::Str("baz".into()),
                    (1, 11)
                ))),
                operator: TokenInfo::new(Token::BangEqual, (1, 8))
            })])
        )
    }
}

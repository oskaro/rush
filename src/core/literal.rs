use crate::core::tokens::{Token, TokenInfo};

use super::position_annotation::PositionAnnotation;

#[derive(Debug, PartialEq, Clone)]
pub enum Literal {
    Str(String),
    Num(f64),
    True,
    False,
    Nil,
}

pub type LiteralInfo = PositionAnnotation<Literal>;

impl TryFrom<TokenInfo> for LiteralInfo {
    type Error = ();
    fn try_from(value: TokenInfo) -> std::prelude::v1::Result<Self, Self::Error> {
        let lit = match value.kind() {
            Token::Literal(l, _) => LiteralInfo::new(l.clone(), value.position()),
            _ => return Err(()),
        };

        Ok(lit)
    }
}

impl From<bool> for Literal {
    fn from(value: bool) -> Self {
        if value {
            Self::True
        } else {
            Self::False
        }
    }
}

impl From<f64> for Literal {
    fn from(value: f64) -> Self {
        Self::Num(value)
    }
}

impl From<String> for Literal {
    fn from(value: String) -> Self {
        Self::Str(value)
    }
}

impl TryInto<String> for Literal {
    type Error = ();
    fn try_into(self) -> std::result::Result<String, Self::Error> {
        match self {
            Self::Str(s) => Ok(s),
            _ => Err(()),
        }
    }
}

impl TryFrom<&Literal> for f64 {
    type Error = ();
    fn try_from(value: &Literal) -> Result<Self, Self::Error> {
        match value {
            Literal::Num(n) => Ok(*n),
            _ => Err(()),
        }
    }
}

impl Into<bool> for Literal {
    fn into(self) -> bool {
        match self {
            Self::True => true,
            Self::False => false,
            Self::Num(n) if n == 0.0 => false,
            Self::Num(_) => true,
            Self::Str(s) if s == "" => false,
            Self::Str(_) => true,
            Self::Nil => false,
        }
    }
}

impl std::fmt::Display for LiteralInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.as_ref().fmt(f)
    }
}

impl std::fmt::Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Str(s) => s.fmt(f),
            Self::Num(n) => write!(f, "{n:?}"),
            Self::True => true.fmt(f),
            Self::False => false.fmt(f),
            Self::Nil => write!(f, "nil"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_that_printing_is_correct() {
        assert_eq!(
            format!("{}", LiteralInfo::new(Literal::Str("hello".into()), (1, 0))),
            "hello"
        );
        assert_eq!(
            format!("{}", LiteralInfo::new(Literal::Num(1234.0), (1, 0))),
            "1234.0"
        );
        assert_eq!(
            format!("{}", LiteralInfo::new(Literal::Num(1234.1234), (1, 0))),
            "1234.1234"
        );
        assert_eq!(
            format!("{}", LiteralInfo::new(Literal::True, (1, 0))),
            "true"
        );
        assert_eq!(
            format!("{}", LiteralInfo::new(Literal::False, (1, 0))),
            "false"
        );
        assert_eq!(format!("{}", LiteralInfo::new(Literal::Nil, (1, 0))), "nil");
    }
}

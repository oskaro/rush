use crate::core::Result;
use std::path::Path;

pub(crate) fn exit(value: Option<i32>) -> ! {
    std::process::exit(value.unwrap_or(0));
}

pub(crate) fn cwd() -> Result<i32> {
    Ok(match std::env::current_dir() {
        Ok(dir) => {
            println!("{}", dir.display());
            0
        }
        Err(e) => {
            println!("cwd: {}", e.to_string());
            1
        }
    })
}

pub(crate) fn cd(path: &Path) -> Result<i32> {
    match std::env::set_current_dir(path) {
        Ok(()) => Ok(0),
        Err(e) => {
            eprintln!("cd: {}", e.to_string());
            Ok(1)
        }
    }
}

pub(crate) fn exec(cmd: crate::core::Command) -> Result<i32> {
    let mut process = match std::process::Command::new(&cmd.program)
        .args(cmd.args)
        .spawn()
    {
        Ok(p) => p,
        Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => {
            eprintln!("exec: Command '{}' not found", &cmd.program);
            return Ok(1);
        }
        Err(e) => {
            eprintln!("exec: {:?}", e);
            return Ok(1);
        }
    };

    let result = match process.wait() {
        Ok(c) => c,
        Err(e) => {
            eprintln!("exec: {:?}", e.to_string());
            return Ok(-1);
        }
    };

    return Ok(result.code().unwrap_or(0));
}

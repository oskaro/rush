use clap::Parser;
use rush::core::session::Session;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    debug: bool,

    #[arg(short, long)]
    command: Option<String>,
}

fn main() {
    let args = Args::parse();
    let session = Session::new(args.debug);

    let status = match args.command {
        Some(c) => session.run_script(&mut c.as_bytes()),
        None => session.start(),
    };

    match status {
        Ok(c) => std::process::exit(c),
        Err(_) => std::process::exit(-1),
    }
}

use anyhow::Context;
use rush::core::ast::AST;
use rush::core::errors::LexError;
use rush::core::interpreter::Interpreter;
use rush::core::lexer::Lexer;
use rush::core::parser::Parser;
use rush::core::tokens::TokenInfo;
use std::fs::File;
use tracing_subscriber::EnvFilter;

use clap::{Parser as ClapParser, Subcommand};
use std::fs;
use std::io::{stdin, stdout, BufRead, BufReader, Cursor, Read, Write};
use std::path::PathBuf;

#[derive(ClapParser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    debug: bool,

    #[command(subcommand)]
    command: Command,
}

#[derive(Debug, Clone, Subcommand)]
enum Command {
    Tokenize { filename: PathBuf },
    Parse { filename: PathBuf },
    Evaluate { filename: PathBuf },
    Interpret,
}

fn tokenize<R: Read>(code: R) -> Result<Vec<TokenInfo>, LexError> {
    let mut bufread = BufReader::new(code);
    let lexer = Lexer::new(&mut bufread);

    lexer
        .tokens()
        .map(|token_result| match token_result {
            Ok(tok) => Ok(tok),
            Err(err) => {
                eprintln!("{err}");
                Err(err)
            }
        })
        .collect::<Result<Vec<TokenInfo>, LexError>>()
}

fn parse(code: &str) -> Result<AST, LexError> {
    let mut reader = Cursor::new(code);
    let lexer = Lexer::new(&mut reader);
    let parser = Parser::with_lexer(lexer);
    Ok(parser.parse().expect("parsing code"))
}

fn evaluate(code: &str) {
    let ast = parse(code).expect("parsing code");
    let interpreter = Interpreter::new(ast);
    let exprs = interpreter.evaluate();
    for expr in exprs {
        let obj = expr.unwrap_or_else(|e| {
            eprintln!("{e}");
            std::process::exit(70);
        });

        if let Some(obj) = obj {
            println!("{}", obj);
        };
    }
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    if args.debug {
        println!("Enabling tracing");
        let filter = EnvFilter::from_default_env();
        tracing_subscriber::fmt()
            .pretty()
            .with_env_filter(filter)
            .init();
    }
    match args.command {
        Command::Tokenize { filename } => {
            let file = File::open(filename).expect("opening provided file");
            let tokens = tokenize(&file).expect("tokenization should work");
            for token in tokens {
                println!(
                    "{},{}:\t{: <11}\t{}",
                    token.position().0,
                    token.position().1,
                    token.name(),
                    token.lexeme(),
                );
            }
        }
        Command::Parse { filename } => {
            let file_contents =
                fs::read_to_string(filename).context("Failed to read user provided file")?;
            let ast = parse(&file_contents).expect("parsing should succeed");
            println!("{ast}");
        }
        Command::Evaluate { filename } => {
            let file_contents =
                fs::read_to_string(filename).context("Failed to read user provided file")?;

            evaluate(&file_contents);
        }
        Command::Interpret => {
            let stdin = stdin().lock();
            let mut stdout = stdout().lock();
            write!(stdout, "\n>>> ")?;
            stdout.flush()?;
            for line in stdin.lines() {
                evaluate(&line?);
                write!(stdout, "\n>>> ")?;
                stdout.flush()?;
            }
        }
    };

    Ok(())
}
